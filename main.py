#!/usr/bin/env/ python
# -*- coding: utf-8 -*-
import re
import requests


# -- globals --
url = "http://XXX.XXX.XXX.XXX/api/persons/"


# -- errors ---
class WrongNameException(Exception):
    pass


class WrongUrlException(Exception):
    pass


# -- function --
def into_json(surname, firstName, snils) -> dict:
    """Запись в словарь для для последующей отправки POST запросом."""
    if not all(map(lambda val: re.fullmatch(r'^[а-яА-Я-]*$', val), [surname, firstName])):
        raise WrongNameException(
            "surname и firstName могут содержать только кирилицу и символ \"-\".")

    my_json = {
        "surname": surname,
        "firstName": firstName,
        "snils": snils
    }
    return my_json


def send_post(my_json: dict) -> requests:
    """Отправляет POST заспрос и возвращает ответ."""
    global url
    # Проверка url по шаблону
    if re.fullmatch(r'^http:\/\/XXX\.XXX\.XXX\.XXX\/api\/persons\/$', url) is None:
        raise WrongUrlException("Введен неверный запрос.")
    return requests.post(url, json=my_json)


# -- launch --
def main():
    my_json = into_json("Сергей", "Викторович", "54724695726")
    responce = send_post(my_json)
    print(responce.status_code)


if __name__ == "__main__":
    main()
