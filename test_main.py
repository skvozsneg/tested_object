#!/usr/bin/env/ python
# -*- coding: utf-8 -*-
import main
import pytest


# -- good test cases --
class TestGoodCases:
    def test_surname_firstName_correсt(self):
        assert isinstance(main.into_json(
            "Дуров", "Павел", "78945612378"), dict)


    def test_status_code_200(self):
        responce = main.send_post(main.into_json("Дуров", "Павел", "78945612378"))
        assert responce.json()["status"]  == 200


    def test_status_code_400_snils(self):
        ERROR_MESSAGE = "Не пройдена проверка на стороне сервиса Цифрового Профиля. Snils validation. Bad control sum"
        responce = main.send_post(main.into_json("Дуров", "Павел", "iamnotcorrectsnils"))
        assert responce.json()["status"]  == 400 and responce.json()["message"] == ERROR_MESSAGE


    def test_status_code_400_persons(self):
        ERROR_MESSAGE = "Персона с данным СНИЛС уже существует. Значение СНИЛС:"
        responce = main.send_post(main.into_json("Дуров", "Павел", "78945612378"))
        assert responce.json()["status"]  == 400 and responce.json()["message"] == ERROR_MESSAGE


# -- bad test cases -- 
class TestBadCases:
    def test_incorrect_surname(self):
        with pytest.raises(main.WrongNameException):
            main.into_json("Du rov", "Павел", "78945612378")

    def test_incorrect_firstName(self):
        with pytest.raises(main.WrongNameException):
            main.into_json("Дуров", "P-a-v-e-l", "78945612378")

    def test_incorrect_url(self):
        main.url = 'https://tg.com/'
        with pytest.raises(main.WrongUrlException):
            main.send_post(...)


# -- auto tests --
class TestAuto:
    @pytest.mark.parametrize("surname, firstName, expected_result", [
        (('Павел'), ('Дуров'), (True)),
        (('Санта-Мария-Герра'), ('Елена-Хулина'), (True)),
        (('ШШШшшоыщшозщшоЗЩшЗШОЗЩШЛЫТФЛЫОИВРОЫпорыпвофывфывф-в-фы-вф-а-а-ф-выа-ыловалыовалоыфвв---выа-фыв-----Вв-'),
         ('офдлывлдоралфорыгфрыушгарщфшгывраофиыовиашоциушагифшгциуашгвифшоывОЫОХхззЗЗХХЪХашгфиушиамщгфшциуашгицуафтволатлдфыв--ва-фыва--фыва----фып-вп--фыв-афыва-ы-ва-фыав-ыав-'), (True)),
        (('------------------------------------------'),
         ('--------------------------------------'), (True))
    ])
    def test_surname_firstName_correсt(self, surname, firstName, expected_result):
        res = isinstance(main.into_json(
            surname, firstName, "randomsnils"), dict)
        assert res == expected_result


    @pytest.mark.parametrize("surname, firstName, snils, expected_result", [
        (('Павел'), ('Дуров'), ('78945612378'), (200)),
        (('Санта-Мария-Герра'), ('Елена-Хулина'), ('54752162378'), (200)),
        (('Павел'), ('Дуров'), ('00000000000'), (400)),
        (('Павел'), ('Дуров'), ('wrong_snils_omg'), (400))
    ])
    def test_status_code(self, surname, firstName, snils, expected_result):
        responce = main.send_post(main.into_json(surname, firstName, snils))
        res = responce.json()["status"]
        assert res == expected_result
